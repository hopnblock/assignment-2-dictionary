%define READ_CALL 0
%define WRITE_CALL 1
%define EXIT_CALL 60

%define SYSTEM_IN 0
%define SYSTEM_OUT 1
%define SYSTEM_ERROR 2

section .text

global exit
global string_length
global print_string
global print_error
global print_newline
global print_char
global print_int
global print_uint
global string_equals
global read_char
global read_line
global read_word
global parse_int
global parse_uint
global string_equals

; rdi - error code
; exits the program with set error code
exit:
	mov rax, EXIT_CALL
	syscall

; rdi - null-terminated string pointer
; returns the length of a string
string_length:
	xor rax, rax
	.loop:
		cmp byte[rdi + rax], 0
		je .end
		inc rax
		jmp .loop
	.end:
		 ret

; rdi - null-terminated string pointer
; prints a string to stdout
print_string:
	push rdi
	call string_length
	pop rsi
	mov rdx, rax
	mov rdi, SYSTEM_OUT
	mov rax, WRITE_CALL
	syscall
	ret

; rdi - null-terminated string pointer
; prints a string to stderr
print_error:
	push rdi
	call string_length
	pop rsi
	mov rdx, rax
	mov rdi, SYSTEM_ERROR
	mov rax, WRITE_CALL
	syscall
	ret

; prints a line break char into stdout
print_newline:
	mov rdi, `\n`

; rdi - char code
; prints a char into stdout
print_char:
	push rdi
	mov rax, WRITE_CALL
	mov rdi, SYSTEM_OUT
	mov rsi, rsp
	mov rdx, 1
	syscall
	pop rdi
	ret

; rdi - signed integer number
; prints a signed integer nubmer into stdout
print_int:
	test rdi, rdi
	jge print_uint
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	neg rdi

; rdi - unsigned integer number
; prints an unsigned integer nubmer into stdout
print_uint:
	test rdi, rdi
	jne .not_zero
	mov rdi, '0'
	jmp print_char
	.not_zero:
		sub rsp, 24
		mov rcx, 22
		mov rsi, 10
		mov rax, rdi
		.loop:
			xor rdx, rdx
			div rsi
			add dl, 48
			mov byte[rsp + rcx], dl
			test rax, rax
			je .end
			dec rcx
			jmp .loop
		.end:
			mov byte[rsp + 23], 0
			lea rdi, [rsp + rcx]
			call print_string
			add rsp, 24
			ret

; rdi - null-terminated string pointer (reference)
; rsi - null-terminated string pointer (target)
; returns 1 if strings are equal, otherwise 0
string_equals:
	xor r8, r8
	.loop:
		mov cl, byte[rdi + r8]
		cmp cl, byte[rsi + r8]
		jne .no
		cmp cl, 0
		je .end
		inc r8
		jmp .loop
	.end:
		mov rax, 1
		ret
	.no:
		xor rax, rax
		ret

; reads a char from stdin and returns its code if succeeded, otherwise 0
read_char:
	xor rax, rax
	push rax
	xor rdi, rdi
	mov rsi, rsp
	mov rdx, 1
	syscall
	test rax, rax
	jz .end_or_err
	js .end_or_err
	pop rax
	ret
	.end_or_err:
		pop rax
		xor rax, rax
		ret

; rdi - buffer pointer
; rsi - buffer length
; reads input from stdin into the buffer with set length until `\n` is entered;
; returns the buffer pointe if succeeded and the input fits into the buffer,
; otherwise 0
read_line:
	push rbx
	push r12
	push r13
	mov r12, rdi
	mov r13, rsi
	dec r13
	xor rbx, rbx
	call read_char
	.loop:
		test al, al
		je .done
		cmp rbx, r13
		je .err
		mov byte[r12 + rbx], al
		inc rbx
		call read_char
		cmp al, `\n`
		jne .loop
	.done:
		mov byte[r12 + rbx], 0
		mov rax, r12
		mov rdx, rbx
		jmp .end
	.err:
		xor rax, rax
	.end:
		pop r13
		pop r12
		pop rbx
		ret

; rdi - buffer pointer
; rsi - buffer length
; reads a word from stdin ignoring space chars;
; returns the buffer pointer in rax, the word length in rdx if succeeded and the word fits into the buffer,
; otherwise 0 in rax
read_word:
	push rbx
	push r12
	push r13
	mov r12, rdi
	mov r13, rsi
	xor rbx, rbx
	.skip:
		call read_char
		cmp al, `\t`
		je .skip
		cmp al, `\n`
		je .skip
		cmp al, ` `
		je .skip
	.loop:
		test al, al
		je .done
		cmp rbx, r13
		jge .err
		mov byte[r12 + rbx], al
		inc rbx
		call read_char
		cmp al, `\t`
		je .done
		cmp al, `\n`
		je .done
		cmp al, ` `
		jne .loop
	.done:
		mov byte[r12 + rbx], 0
		mov rax, r12
		mov rdx, rbx
		jmp .end
	.err:
		xor rax, rax
	.end:
		pop r13
		pop r12
		pop rbx
		ret

; rdi - string pointer
; parses an unsigned integer number from the beginning of the string;
; returns the parsed number in rax and number length in rdx if succeeded, 
; otherwise 0 in rdx
parse_uint:
	xor rax, rax
	xor r8, r8
	xor r9, r9
	mov rsi, 10
	.loop:
		mov r8b, byte[rdi + r9]
		cmp r8, '9'
		jg .stop
		sub r8, '0'
		jl .stop
		add rax, r8
		inc r9
		mul rsi
		jmp .loop
	.stop:
		xor rdx, rdx
		test r9, r9
		jnz .complete
		ret
	.complete:
		div rsi
		mov rdx, r9
		ret

; rdi - string pointer
; parses a signed integer number from the beginning of the string;
; returns the parsed number in rax and number length (including the sign) in rdx if succeeded,
; otherwise 0 in rdx
parse_int:
	mov cl, byte[rdi]
	cmp cl, '-'
	je .sign
	cmp cl, '+'
	jne parse_uint
	.sign:
		push rcx
		inc rdi
		call parse_uint
		test rdx, rdx
		jne .ok
		pop rcx
		ret
	.ok:
		pop rcx
		cmp cl, '-'
		jne .positive
		neg rax
	.positive:
		inc rdx
		ret 

; rdi - null-terminated string pointer
; rsi - buffer pointer
; rdx - buffer length
; copies the target string to the buffer;
; returns the string length if it fits into the buffer, otherwise 0
string_copy:
	push rdi
	push rsi
	push rdx
	call string_length
	pop rdx
	pop rsi
	pop rdi
	cmp rax, rdx
	jg .overflow
	xor r8, r8
	.loop:
		mov cl, byte[rdi + r8]
		mov byte[rsi + r8], cl
		test cl, cl
		je .end
		inc r8
		jmp .loop
	.overflow:
		xor rax, rax
		ret
	.end:
		ret
