.PHONY: clean test

main: main.o dict.o lib.o
	ld -o $@ $^

main.o: main.asm colon.inc words.inc
	nasm -felf64 $<

%.o: %.asm %.inc
	nasm -felf64 $<

clean:
	rm -rf *.o main

test: main
	./test.py
