%define next 0

%macro colon 2

	%ifnstr %1
		%error "Argument 1 of colon has to be a string."
	%endif

	%ifnid %2
		%error "Argument 2 of colon has to be an identifier."
	%endif
	
	%2: dq next
	db %1, 0
	
	%define next %2
	
%endmacro
