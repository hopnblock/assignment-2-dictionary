#!/usr/bin/python3

import subprocess

test_inputs = ["itmo", "ryan gosling", "blah blah blah", "", "A"*255, "B"*256]
expected_outputs = ["proga web opd english kik db god save us", "bladerunner 2049", "", "", "", ""]
expected_errors = ["", "", "No such entry.", "No such entry.", "No such entry.", "The input must not be longer than 255 characters."]

main = "./main"
errors = 0

print("Tests are running...")
print("-"*64)
for i in range(len(test_inputs)):
    process = subprocess.run(main, input=test_inputs[i], text=True, capture_output=True)
    if process.stdout == expected_outputs[i] and process.stderr == expected_errors[i]:
        continue
    print()
    errors += 1
    print("Test", i + 1, "failed!")
    print("Input:", test_inputs[i])
    if expected_outputs[i]:
        print("Expected:", expected_outputs[i])
        print("Got:", process.stdout)
    else:
        print("Expected:", expected_errors[i])
        print("Got:", process.stderr)

print()    
print("-"*64)
print("Tests completed.")
if errors:
    print("Total tests failed:", errors)
else:
    print("All tests passed!")
