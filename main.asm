%include "colon.inc"
%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define BUFFER_LENGTH 256 ; 255 for the input, plus the null-terminator

section .rodata
input_too_long_message: db "The input must not be longer than 255 characters.", 0
no_entry_message: db "No such entry.", 0

section .bss
input_buffer: resb BUFFER_LENGTH

global _start

section .text

_start:
	mov rdi, input_buffer
	mov rsi, BUFFER_LENGTH
	call read_line
	test rax, rax
	jnz .input_ok
	mov rdi, input_too_long_message
	jmp .print_error_and_exit
	.input_ok:	
		mov rdi, rax
		mov rsi, DICT
		call find_word
		test rax, rax
		jnz .found
		mov rdi, no_entry_message
	.print_error_and_exit:
		call print_error
		jmp exit
	.found:
		mov rdi, rax
		call get_value
		mov rdi, rax
		call print_string
		jmp exit
