%include "lib.inc"

section .text

global find_word
global get_value

%define LABEL_SIZE 8

; rdi - key string to look for
; rsi - dict pointer
; Searches the dict for the key string,
; returns the address of the entry if found, otherwise 0
find_word:
	push r12
	push r13
	mov r12, rdi
	mov r13, rsi
	.loop:
		test rsi, rsi
		jz .not_found
		add rsi, LABEL_SIZE
		call string_equals
		test rax, rax
		jnz .found
		mov rdi, r12
		mov r13, [r13]
		mov rsi, r13
		jmp .loop
	.found:
		mov rax, r13
		add rax, LABEL_SIZE
		jmp .end
	.not_found:
		xor rax, rax
	.end:
		pop r13
		pop r12
		ret

; rdi - pointer to the key string of the dict entry
; returns a pointer to the value for the key
get_value:
	push rdi
	call string_length
	pop rdi
	add rax, rdi
	inc rax
	ret
